package starter.stepdefinitions;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.specification.RequestSpecification;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Steps;
import org.hamcrest.Matchers;

import static net.serenitybdd.rest.SerenityRest.restAssuredThat;
import static org.hamcrest.Matchers.contains;

public class SearchStepDefinitions {

	@Steps
	public CarsAPI carsAPI;

//    @When("he calls endpoint {string}")
//    public void heCallsEndpoint(String arg0) {
//        SerenityRest.given().get(arg0);
//    }

//    @Then("he sees the results displayed for apple")
//    public void heSeesTheResultsDisplayedForApple() {
//        restAssuredThat(response -> response.statusCode(200));
//    }
//
//    @Then("he sees the results displayed for mango")
//    public void heSeesTheResultsDisplayedForMango() {
//        restAssuredThat(response -> response.body("title", contains("mango")));
//    }
//    
//    @Then("he doesn not see the results")
//    public void he_Doesn_Not_See_The_Results() {
//        restAssuredThat(response -> response.body("error", contains("True")));
//    }

	// Common stepDef for both scenario
	@When("she calls endpoint {string}")
	public void sheCallsEndpoint(String arg0) {
		SerenityRest.given().get(arg0);
	}

	// Positive Scenario
	@Then("she sees the results displayed for Dutch Tofu 550 g")
	public void sheSeesTheResultsDisplayedForTofu() {
		SerenityRest.restAssuredThat(response -> response.extract().path("x[0].title", "Dutch Tofu 550 g"));

	}

	// Negative Scenario
	@Then("she should not see any results displayed for Kiwi")
	public void She_Doesn_Not_See_The_Results_Kiwi() {
		restAssuredThat(response -> response.statusCode(404));

	}
}
