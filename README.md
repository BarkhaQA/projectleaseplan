Brief summary of the pointers as required is below: 

1. First I downloaded the project using the google drive shared link and put it locally on my system I first added it as a maven project in Eclipse IDE using the following steps -
File >> Import >> Maven >> Existing MAven Project >> browse the path to the Root Directory where locally the project is added.


2. Then I started running the Feature file using Cucumber, but it did not work for me. 
So I tried running the project using terminal by using the command - "mvn Clean Verify" and received the error on the run of Java version, 
since my system has the Jdk 19 installed so I lower down the jdk version and run the project again and recieved the error as below - 

  [ERROR] Failures: 
[ERROR]   1 expectation failed.
JSON path title doesn't match.
Expected: iterable containing ["mango"]
  Actual: <[AH Ananas mango, Olvarit Appel perzik en mango 12+ maanden, Fuze Tea Green tea mango chamomile, Johma Kip-mango salade, Dubbeldrank Mango & passievrucht, AH Mango, AH Mango, DubbelFrisss 1Kcal Ananas & mango, Maaza Mango drink, AH Mango sticks, AH Biologisch Mango, Dubbeldrank Mango-guanaba, Coolbest Mango dream 30% minder suiker sap, AH Doosje met mangostukjes, AH Mango eetrijp, Sourcy Vitaminwater mango, CoolBest Mango dream, Pickwick Rooibos mango & peach, Dubbeldrank Mango & guanabana, Sisi No bubble mango PET fles, Fuze Tea Green mango chamomile tray, Optimel Drinkyoghurt mango passievrucht, DubbelDrank Mango & guanabana, Del Monte Mango Schijfjes, AH Mango, Bonbébé Fruithapje perzik appel mango, AH Zakje met mango stukjes, Appelsientje Dubbeldrank mango passievrucht, AH Mango, Mogu mogu Mango, AH Biologisch Mango, Bonbébé Pouch appel-mango, Patak's Mango chutney, Mango duo eetrijp, AH Drinkyoghurt passievrucht mango, Look-O-Look Zure Matten mango aardbeismaak, Appelsientje Mango volle smaak, Fuze Tea Mango chamomile, EAT ME Mango, Tropical Aloe mango, Sisi No bubbles mango, Appelsientje Mango volle smaak, Servero 100% Fruit to Go Mango Therapy 4 x 90 g, Topo Chico Tropical mango, CoolBest Mango dream]>


3. To debug it I used the Postman API tool to check the API which you can refer to in the "LeasePlan Api Check.docx" Word file. 
Then tried different rest assured options to fix this and added the new scenarios as requiured for this assignment.

4. I clean up the code by commenting it in order to run my positive and negative scenarios which I have added in the "SearchStepDefinitions" file from line 41-59.
In this I added a common class of "When" to call the end points followed by the positive and negative scenarios
I also added the Positive and negative scenario under the "Feature" file as well from lines - 19-26.

5. My positive scenario consists of the test for TOFU in that using the postman api tool I extract the path and run the scenario for index 0 
My negative scenario consists of random names like KIWI which is showing the error code of 404.

6. After running this code I received the the build success but found that the report was not generating.

7. I again run the project using the command - mvn Serenity:aggregate and then run the project using mvn Verify and the report file - Index.html got added in this location target/site/serenity/index.html.


For "GitLab CI"

1. I have installed the myrunner on windows and created a CI pipeline. This process consist of below steps - 
downloaded the binary file for my required system >> using the CMD I installed it >> Register it >> give the start command to start my runner.

2. For every commit the pipeline is getting triggered.
for commiting the file I use gitbash by right clicking on the project location and selecting GitBash option with following commands - 
git pull - to pull all the latest changes
git add . - to add the file 
git commit -m"commit message" - to commit 
git push - to push to Gitlab

3. Since the validation of account has not been done using the Credit card information, on commit i am getting the failure of pipeline. 
Treid multiple options but validation is mandatory. Refer the screenshots in the word document - LeasePlan Api Check.docx





